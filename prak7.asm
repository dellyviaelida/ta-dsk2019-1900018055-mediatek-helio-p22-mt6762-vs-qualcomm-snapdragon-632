.MODEL SMALL ;tanda directive ini digunakan untuk memberitahu kepada assembler bentuk memori yang digunakan oleh program kita. SMALL digunakan jika data yang digunakan oleh program kurang dari 64 kb
.CODE ;tanda directive ini digunakan untuk memberitahu kepada assembler bahwa kita akan mulai menggunakan code segmentnya disini
org 100h ;perintah ini digunakan untuk memberitahukan assembler agar saat program dijalankan ditaruh mulai pada offset ke 100h (256 byte)
proses: ;memulai proses assembly
	jmp start ;perintah untuk melakukan lompatan ke tempat yang ditunjukkan oleh perintah yaitu ke start
	oldX dw -1 ;oldX = -1
	oldY dw 0 ;oldX = 0
start:
	mov ah, 00 ;perintah untuk mengubah ke mode grafik
	mov al, 13h ;perintah untuk menampilkan layar 256 warna, berukuran 320x200 pixel
	int 10h ;perintah untuk mengosongkan kursor mouse
	mov ax, 0 ;perintah untuk mereset status mouse saat ini
	int 33h ;perintah untuk mengambil kendali penuh atas mouse dan informasi yang disediakan oleh program driver mouse
	cmp ax, 0 ;perintah untuk membandingkan ax dengan 0
	mov ax, 1 ;perintah untuk menampilkan kursor mouse
	int 33h ;perintah untuk mengambil kendali penuh atas mouse dan informasi yang disediakan oleh program driver mouse
check_mouse_button:
	mov ax, 3 ;perintah untuk mengembalikan posisi kursor mouse saat ini
	int 33h ;perintah untuk mengambil kendali penuh atas mouse dan informasi yang disediakan oleh program driver mouse
	shr cx, 1 ;pada mode ini nilai dari CX berlipat ganda 
	cmp bx, 1 ;perintah untuk membandingkan bx dengan 1
	jne xor_cursor ;jump not equal perintah untuk lompat ke xor_cursor jika perbandingan antara kedua nilai tidak sama
	mov al, 1010b ;memberikan warna pada pixel
	jmp draw_pixel ;perintah untuk melakukan lompatan ke tempat yang ditunjukkan oleh perintah yaitu ke draw_pixel
xor_cursor:
	cmp oldX, -1 ;perintah untuk membandingkan oldX dengan -1
	je not_required ;jump equal, untuk lompat ke not_required jika perbandingan antara kedua nilai sama
	push cx ;perintah untuk menyimpan nilai cx pada stack
	push dx ;perintah untuk menyimpan nilai dx pada stack
	mov cx, oldX ;cx:= oldX
	mov dx, oldY ;dx:= oldY
	mov ah, 0dh ;perintah untuk mendapatkan pixel
	int 10h ;perintah untuk mengosongkan kursor mouse
	xor al, 1111b ;perintah untuk memberikan warna pada pixel
	mov ah, 0ch ;set pixel
	int 10h ;perintah untuk mengosongkan kursor mouse
	pop dx ;perintah untuk mengeluarkan nilai dx di stack ke dalam sebuah register
	pop cx ;perintah untuk mengeluarkan nilai cx di stack ke dalam sebuah register
not_required:
	mov ah, 0dh ;perintah untuk mendapatkan pixel
	int 10h ;perintah untuk mengosongkan kursor mouse
	xor al, 1111b ;perintah untuk memberikan warna pada pixel
	mov oldX, cx ;cx bernilai oldX
	mov oldY, dx ;dx bernilai oldY
draw_pixel:
	mov ah, 0ch ; set pixel
	int 10h ;perintah untuk mengosongkan kursor mouse
check_esc_key:
	mov dl, 255
	mov ah, 6
	int 21h ;perintah untuk mencetak variabel
	cmp al, 27 ;perintah untuk esc
	jne check_mouse_button ;jump not equal, perintah untuk lompat ke check_mouse_button jika perbandingan antara kedua nilai tidak sama
stop:
	mov ax, 2 ;perintah untuk membuat kursor mouse tidak terlihat (tersembunyi)
	int 33h ;perintah untuk mengambil kendali penuh atas mouse dan informasi yang disediakan oleh program driver mouse
	mov ax, 3 ;perintah untuk kembali ke text mode 80x25
	int 10h ;perintah untuk mengosongkan kursor mouse
	;menampilkan kursor tesk berbentuk kotak berkedip:
	mov ah, 1
	mov ch, 0
	mov cl, 8
	int 10h ;perintah untuk mengosongkan kursor mouse
	mov dx, offset msg ;perintah untuk mengambil alamat offset msg
	mov ah, 9 ;perintah untuk mendefenisikan ulang tampilan kursor mouse saat layar berada dalam mode grafis
	int 21h ;perintah untuk mencetak variabel
	mov ah, 0
	int 16h ;perintah untuk menyimpan penempatan mouse yang baru
	ret ;return, untuk kembali ke posisi kode dimana fungsi dipanggil
msg db " press any key.... $" ;mendeklarasikan msg yang berisi press any key
end proses ;mengakhiri proses