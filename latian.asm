.model small
.code
org 100h
start:
	jmp cetak
	hello	db 'DELLYVIA ELIDA '
		db '1900018055 '
		db 'B'
		db '$'
cetak:
	mov ah, 09h; mencetak string ASCII
	mov dx, offset hello
	int 21h; mencetak program
habis:
	int 20h; memberhentikan program
end start; mengakhiri program